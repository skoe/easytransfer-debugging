
Test 6:

- CPLD programmieren, jetzt so:
  - `easp -p 0x8738 ef3-no-vic.svf`
- Rest wie unten
- Zähler zählt wieder wie bei Test 2, aber BA und Phi2 wird bei Lesezugriffen zusätzlich abgeprüft.

Test 5:

- CPLD programmieren, jetzt so:
  - `easp -p 0x8738 ef3-count-evilness.svf`
- Rest wie unten
- Zähler zählt jetzt "010" und "101" im Phi2-Schieberegister. Sollte 0 bleiben.

=> Zähler blieb auf 0, Phi2 scheint sauber zu sein.

Test 4:

- CPLD programmieren, jetzt so:
  - `easp -p 0x8738 ef3-more-sync2.svf`
- Menü `ef3-menu-with-counter.crt` sollte noch drin sein (siehe zweiter Test).
- Durchführung und Auswertung auch wie im zweiten Test.

=> Zähler war n * 3 Bytes zu hoch, aber realistisch

Dritter Test:

- CPLD programmieren:
  - `easp -p 0x8738 ef3-more-sync.svf`
- Menü `ef3-menu-with-counter.crt` sollte noch drin sein (siehe zweiter Test).
- Durchführung und Auswertung auch wie im zweiten Test.

=> Zähler war komplett daneben, evtl. Sollwert * 2 + n * 3

Zweiter Test:

- CPLD programmieren:
  - Jumper auf "PROG", C64 anschalten
  - `easp -p 0x8738 ef3-glitch-counter.svf`
  - Jumper in die "DATA"-Position zurückstecken
- Mit EF3-Menütaste in das EF3-Menü wechseln
- Menü mit Counter-Ausgabe flashen:
  - `ef3xfer -c ef3-menu-with-counter.crt`
  - Auf System Area flashen
- Wieder Mit EF3-Menütaste in das EF3-Menü wechseln
  - `ef3xfer -x testfile/testfile.prg`

Das passiert:

- C64 lädt nur die ersten 600 der 800 Bytes aus der Datei
- und gibt zwei Zähler in der unteren Ecke aus:
  - Zähler vor dem Übertragen der Datei
  - Zähler nach dem Übertragen
  - Die Differenz sollte 2 (Size) + 2 (Addr) + 600 Bytes sein

Beispiel:

![Beispiel](res/600bytes.png "Beispiel")
