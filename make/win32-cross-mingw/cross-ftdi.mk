#
# This software is provided 'as-is', without any express or implied
# warranty.  In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# Thomas Giesel skoe@directbox.com
#

here := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))

libusb        := libusb-1.0.24
libftdi       := libftdi1-1.5

# major must be plus 1
libftdi_major_ver := 2
libftdi_minor_ver := 5

libftdi_src :=
libftdi_src += $(archive_dir)/$(libftdi)/src/ftdi.c
libftdi_src += $(archive_dir)/$(libftdi)/src/ftdi_stream.c

# also used in the top-level Makefile
cxxflags      += -I $(tmp_install)/include/libusb-1.0
cxxflags      += -I $(archive_dir)/$(libftdi)/src
cflags        += -I $(tmp_install)/include/libusb-1.0
cflags        += -I $(archive_dir)/$(libftdi)/src
cxxlibs       += -L $(tmp_install)/lib -L $(tmp_install)/bin
cxxlibs       += -l ftdi1 -lusb-1.0.dll
clibs         += -L $(tmp_install)/lib -L $(tmp_install)/bin
clibs         += -l ftdi1 -lusb-1.0.dll

###############################################################################
#
$(archive_dir)/$(libusb)/configure:
	cd $(archive_dir) && tar xf $(libusb).tar.bz2

###############################################################################
#
$(outbase)/libusb/Makefile: $(archive_dir)/$(libusb)/configure
	mkdir -p $(outbase)/libusb
	-make -C $(libusb_dir) distclean # wtf?
	cd $(outbase)/libusb && $(archive_dir)/$(libusb)/configure \
		CFLAGS=-static-libgcc -disable-static --enable-shared \
		--prefix=$(tmp_install) --host=$(host)

###############################################################################
#
.PHONY: libusb
libusb: $(outbase)/libusb.done

$(outbase)/libusb.done: $(outbase)/libusb/Makefile
	$(MAKE) -C $(outbase)/libusb install
	touch $@

###############################################################################
#
$(libftdi_src):
	cd $(archive_dir) && tar xf $(libftdi).tar.bz2

###############################################################################
#
.PHONY: libftdi
libftdi: $(tmp_install)/bin/libftdi1.dll

$(tmp_install)/bin/libftdi1.dll $(tmp_install)/lib/libftdi1.dll.a &: \
		$(libftdi_src) libusb
	$(cc) -DNDEBUG -shared -o $(tmp_install)/bin/libftdi1.dll \
		$(cflags) $(ldflags) -I $(here) \
		-Wl,--out-implib,$(tmp_install)/lib/libftdi1.dll.a \
		-Wl,--major-image-version,$(libftdi_major_ver),--minor-image-version,$(libftdi_minor_ver) \
		$(libftdi_src) \
		-L $(tmp_install)/lib -L $(tmp_install)/bin \
		-l usb-1.0.dll
